﻿using UnityEngine;
using System.Collections;

public struct CheckPointData
{
    public CheckPointData(Vector3 position, Quaternion rotation)
    {
        this.position = position;
        this.rotation = rotation;

    }

    public Vector3 position;
    public Quaternion rotation;

}

[RequireComponent(typeof(NetworkView))]
public class Checkpoint : MonoBehaviour {

	public bool propellerEnabled;
	public bool torpedoEnabled;
	public bool lightEnabled;
	public bool repairEnabled;

    void OnTriggerEnter(Collider collider)
    {
        if (!Network.isServer)
            return;

        GameMaster gameMaster = GameMaster.instance;

        if (collider.CompareTag("Submarine"))
        {
            int propellerBit = propellerEnabled ? 1 << 0 : 0 << 0;
            int torpedoBit = torpedoEnabled ? 1 << 1 : 0 << 1;
            int lightBit = lightEnabled ? 1 << 2 : 0 << 2;
            int repairBit = repairEnabled ? 1 << 3 : 0 << 3;
            Submarine.current.networkView.RPC("UpdateModules", RPCMode.All, propellerBit | torpedoBit | lightBit | repairBit);
            if (gameMaster.lastCheckpoint.position != transform.position)
            {
                gameMaster.lastCheckpoint = new CheckPointData(transform.position, transform.rotation);
                gameMaster.SaveScore();
                networkView.RPC("OnCheckpointPassed", RPCMode.All);
            }
        }
    }

    [RPC]
    void OnCheckpointPassed()
    {
        GameMaster gameMaster = GameMaster.instance;

        if (gameObject.name == "CheckpointTorpedo")
        {
            if (gameMaster.firstTimeTorp == true && gameMaster.gameIsOngoing)
            {
                gameMaster.firstTimeTorp = false;
                TutorialUI.instance.torpedo = true;
            }
        }
        else if (gameObject.name == "CheckpointLight")
        {
            if (gameMaster.firstTimeLightFlare == true && gameMaster.gameIsOngoing)
            {
                gameMaster.firstTimeLightFlare = false;
                TutorialUI.instance.light = true;
            }
        }

        InGameUIManager.instance.ShowCheckpointMesesage();
    }

}
