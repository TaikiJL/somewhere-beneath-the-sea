﻿using UnityEngine;
using System.Collections;

public class AmbianceZone : MonoBehaviour {

	[SerializeField] private bool fogOff=false;

    public enum ZoneType
    {
        StartBattleThemeZone,
        StopBattleThemeZone,
        DarknessIncreaseZone,
        DarknessDecreaseZone,
		FogDensityDecreaseZone
    }

    public ZoneType zoneType;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Submarine"))
        {
            switch (zoneType)
            {
                case ZoneType.StartBattleThemeZone:
                    AudioManager.instance.StartBattle();
                    break;
                case ZoneType.StopBattleThemeZone:
                    AudioManager.instance.StopBattle();
                    break;
                case ZoneType.DarknessIncreaseZone:
                    GameMaster.instance.increaseDarkness(fogOff);
                    break;
                case ZoneType.DarknessDecreaseZone:
                    GameMaster.instance.decreaseDarkness();
                    break;
				case ZoneType.FogDensityDecreaseZone:
					GameMaster.instance.decreaseFog();
					break;
                default:
                    break;
            }
        }
    }

}
