﻿using UnityEngine;
using System.Collections;

public class LevelGoal : MonoBehaviour {

    void OnTriggerEnter(Collider collider)
    {
        if (!Network.isServer)
            return;

        if (collider.CompareTag("Submarine"))
        {
            GameMaster.instance.networkView.RPC("LevelCompleted", RPCMode.All);
        }
    }

}
