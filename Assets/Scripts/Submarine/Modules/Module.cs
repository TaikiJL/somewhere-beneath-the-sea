﻿using UnityEngine;
using System.Collections;

public abstract class Module : MonoBehaviour {

    public virtual void OnSelected() {}
    public virtual void OnUnselected() { StopUse(); }
    public virtual void Use() {}
    public virtual void StopUse() {}
	public virtual float GetType() { return 0;}
    public virtual void SendAxis(float horizontal, float vertical) {}

}