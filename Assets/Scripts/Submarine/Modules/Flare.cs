﻿using UnityEngine;
using System.Collections;

/// <Martin>
/// Code for torpedo.

///To Do: 
/// -Replication On The Server
/// -Explosion animation for the Torpedo to replace instant destruction.

public class Flare : MonoBehaviour {
	
	//Public Vars
	public float maxAliveTime = 20.0f;
    public float aliveTimeAfterCollision = 8.0f;

	public int emission=100;

	//Private Vars

	private float aliveTime;
	
	private bool isServer;
	
	private Vector3 truePosition;
	private Quaternion trueRotation;
	
	// Use this for initialization
	void Start () {
		isServer = Network.isServer;
		rigidbody.isKinematic = !isServer;
        rigidbody.useGravity = false;
		//renderer.material.SetFloat ("_EmissionLM", emission);
		
		aliveTime = 0.0f;
		
		truePosition = transform.position;
		trueRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isServer)
		{
			transform.position = Vector3.Lerp(transform.position, this.truePosition, Time.deltaTime * 5);
			transform.rotation = Quaternion.Lerp(transform.rotation, this.trueRotation, Time.deltaTime * 5);
			return;
		}
		
		aliveTime = aliveTime + Time.deltaTime;
		//Destroy any torpedo that has been out for too long to release memory.
		if (aliveTime >= maxAliveTime){
			networkView.RPC("Explode", RPCMode.All);
		}
	}

	void OnCollisionEnter(Collision other){
        if (Network.isServer)
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.useGravity = true;
            rigidbody.drag = 6.0f;
            if (aliveTime < maxAliveTime)
                aliveTime = maxAliveTime - aliveTimeAfterCollision;
        }
	}
	
	//Position streaming from server to client
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		if (stream.isWriting)
		{
			truePosition = transform.position;
			stream.Serialize(ref truePosition);
			
			trueRotation = transform.rotation;
			stream.Serialize(ref trueRotation);
		}
		else
		{
			stream.Serialize(ref truePosition);
			stream.Serialize(ref trueRotation);
		}
	}
	
	//Generate explosion
	[RPC] void Explode(){
		//Insert effect here
		
		Destroy(gameObject);
	}
}
