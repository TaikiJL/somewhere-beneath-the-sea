﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Light that can be toggled on/off.
/// </summary>
public class LightModule : AimableModule {

	[SerializeField] private float lightActivationDistance;
	[SerializeField] private float lightActivationRadius;

	private Light light;
	private bool used;

	private RaycastHit hit;
	// Use this for initialization
	void Start () {
		light = transform.GetComponentInChildren<Light> ();
		used = false;
		light.enabled = false;
        base.Initialize();
	}

    void Update()
    {
        int test = 1 << 11;
        base.UpdateModuleRotation();
		Vector3 front = transform.TransformDirection (Vector3.forward);
		if ((light.enabled == true) && Physics.SphereCast(transform.position, lightActivationRadius,transform.forward, out hit,lightActivationDistance,test))
		{

			if (hit.collider.gameObject.name  == "AnglerFish")
				hit.collider.GetComponent<AnglerFish>().kappa.enabled=true;
			else if (hit.collider.gameObject.CompareTag("Flower"))
				hit.collider.gameObject.GetComponentInChildren<LightScript>().ActivateLight();
		}
	

    }

    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        base.StreamRotation(stream);
    }

	public override float GetType(){
		return 2;
	}

	public override void Use()
    {
		//This is for when it will be attached to Submarine.
        used = true;
		light.enabled = true;
	}

    public override void StopUse()
    {
		used = false;
		light.enabled = false;
    }

    public override void SendAxis(float horizontal, float vertical)
    {
        base.RotateCamera(horizontal, vertical);
    }

}
