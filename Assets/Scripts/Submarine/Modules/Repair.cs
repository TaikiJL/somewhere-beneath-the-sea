﻿using UnityEngine;
using System.Collections;

public class Repair : Module {

    [SerializeField] private float repairSpeed = 2.0f;
    [SerializeField] private float speedMultiplier = 2.0f;

    private byte repairCount = 0;
    private bool repair = false;
    private float currentMultiplier = 1.0f;
    private Submarine submarine;
	private bool doVibrate=false;

    void Start()
    {
        submarine = GetComponent<Submarine>();
    }

    private IEnumerator RepairSubmarine()
    {
        while (repair)
        {
            submarine.health += repairSpeed * currentMultiplier * Time.deltaTime;
            yield return null;
        }
    }

	public override float GetType(){
		return 3;
	}

    public override void Use()
    {
		if(submarine.health<100.0f)
			doVibrate = true;
		StartCoroutine (RepairVibrations ());
        if (!Network.isServer)
            return;   

        if (repairCount < 2)
            ++repairCount;
        if (repairCount < 2)
        {
            repair = true;
            StartCoroutine(RepairSubmarine());
        }
        else
        {
            currentMultiplier = speedMultiplier;
        }
    }

	private IEnumerator RepairVibrations(){
		while (doVibrate && submarine.health <100.0f)
		{
			GamePadManager.instance.Vibrate(1.0f, 1.0f, 0.3f);
			AudioManager.instance.playRepair();
			yield return new WaitForSeconds(1.0f);
		}
	}

    public override void StopUse()
    {
		doVibrate = false;
        if (!Network.isServer)
            return;

        if (repairCount > 0)
            --repairCount;

        if (repairCount < 1)
        {
            repair = false;
        }
        else
        {
            currentMultiplier = 1.0f;
        }
    }

    public override void SendAxis(float horizontal, float vertical)
    {
        
    }

}