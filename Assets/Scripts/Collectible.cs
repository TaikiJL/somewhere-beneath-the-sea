﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NetworkView))]
public class Collectible : MonoBehaviour
{

    #region Inspector variables

    [SerializeField]
    private int _rewardPoints = 10;

    #endregion

    #region Events

    public static event System.Action<int> OnScoreChanged;

    #endregion

    void OnTriggerEnter(Collider collider)
    {
        if (Network.isServer && collider.CompareTag("Submarine"))
        {
            networkView.RPC("OnCollected", RPCMode.All);
        }
    }

    [RPC]
    void OnCollected()
    {
        GameMaster.instance.score += _rewardPoints;
        OnScoreChanged(GameMaster.instance.score);
        Destroy(gameObject);
    }

}
