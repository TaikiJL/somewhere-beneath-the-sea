﻿using UnityEngine;
using System.Collections;

public class Windforce : MonoBehaviour {

	[Range(10,300)]
	[SerializeField] private float _force = 200.0f;
	
	public float force{get{return _force;}}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float particleSpeed = _force*0.035f;
        ParticleSystem particles = GetComponent<ParticleSystem>();
        if (particles != null)
            particles.startSpeed = Random.Range(particleSpeed, particleSpeed * 2);
	}
}
