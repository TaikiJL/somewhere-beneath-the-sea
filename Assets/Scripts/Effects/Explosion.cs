﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	private float duration = 2.0f;
	private float destructionTimer = 0.0f;

	// Use this for initialization
	void Start () {
		//duration = GetComponent<AnimationClip>().length;
	}
	
	// Update is called once per frame
	void Update () {
		destructionTimer += Time.deltaTime;

		if (destructionTimer >= duration){
			Destroy(gameObject);
		}
	}
}