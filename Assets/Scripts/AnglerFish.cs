﻿using UnityEngine;
using System.Collections;

public class AnglerFish : MonoBehaviour {

	public float maxDistance = 15f;
	public float speed = 1.5f;
	public float maxSpeed = 3f;
	public float offsetInFront = -1f;
	
	internal Light kappa;

	private GameObject sub;
	private Vector3 submarinePos;
	private float startSpeed;

	private Vector3 truePosition;
	private Quaternion trueRotation;
	private float lastDist;
	private bool isAttacking;

	// Use this for initialization
	void Start () {
		kappa = transform.GetComponentInChildren<Light>();
		kappa.enabled = false;
	}
	
	//Update is called once per frame
	void Update () {
		if(Submarine.current)
			submarinePos = Submarine.current.transform.position;

		if(Network.isServer){
			float dist = Vector3.Distance (transform.position, submarinePos);

		    if (dist < maxDistance){
				Vector3 direction =  Vector3.Normalize(new Vector3(submarinePos.x + offsetInFront, submarinePos.y, submarinePos.z) - transform.position);
				transform.Translate (direction * Time.deltaTime * speed, Space.World);
				transform.forward = Vector3.Normalize(direction);

				if (speed < maxSpeed){
					speed = speed + Time.deltaTime;
				}
			}
			//créait un bug: des qu'un projectile était lancé, l'angler fish était détruit
			if (lastDist != 0f && dist >= lastDist+0.1f &&isAttacking){
				Destroy(gameObject);
			}
			lastDist = dist;
		}

		else{
			transform.position = Vector3.Lerp( transform.position, truePosition, Time.deltaTime * 10.0f );
			transform.rotation = Quaternion.Slerp( transform.rotation, trueRotation, Time.deltaTime * 10.0f );
		}
	}

	//Position streaming from server to client
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		if (stream.isWriting)
		{
			truePosition = transform.position;
			stream.Serialize(ref truePosition);
			
			trueRotation = transform.rotation;
			stream.Serialize(ref trueRotation);
		}
		else
		{
			stream.Serialize(ref truePosition);
			stream.Serialize(ref trueRotation);
		}
	}

	[RPC]
	public void impact(Vector3 impactPos) {
		Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider collision){
		if (collision.CompareTag ("Flare")) {
			kappa.enabled = true;
		}
		if (collision.CompareTag ("Submarine")) {
			isAttacking=true;
		}
	}
}
