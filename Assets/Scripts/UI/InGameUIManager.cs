﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class InGameUIManager : MonoBehaviour {
	
    [System.Serializable]
    private struct HealthColor {
        public Color color;
        public float healthLevel;
    }

	public static InGameUIManager instance { get; private set;}

	[SerializeField] private GameObject _gameOver;
	[SerializeField] private GameObject _finishedGame;
	[SerializeField] private GameObject _background;
	[SerializeField] private GameObject _damageBackground;
	[SerializeField] private GameObject _inGameUI;
	[SerializeField] private GameObject _pause;
    [SerializeField] private Image _crosshair;
    [SerializeField] private Text _score;
    [SerializeField] private HealthColor[] _healthColors;
    [SerializeField] private Text _endGameText;
    [SerializeField] private Text _endGameScore;
    [SerializeField] private Text _endGameTime;
    [SerializeField] private GameObject[] _scoreListLines;

	//health bar variables
	public Slider scrollbar;
	public Image fillColor;
	public Image healthBarBase;
	public Toggle invertToggle;

	public Image A;
	public Image B;
	public Image X;
	public Image Y;

	[SerializeField] private Sprite A_On;
	[SerializeField] private Sprite A_Off;
	[SerializeField] private Sprite B_On;
	[SerializeField] private Sprite B_Off;
	[SerializeField] private Sprite B_NoModule;
	[SerializeField] private Sprite X_On;
	[SerializeField] private Sprite X_Off;
	[SerializeField] private Sprite X_NoModule;
	[SerializeField] private Sprite Y_Flare_On;
	[SerializeField] private Sprite Y_Flare_Off;
	[SerializeField] private Sprite Y_Light_On;
	[SerializeField] private Sprite Y_Light_Off;
	[SerializeField] private Sprite Y_NoModule;

    [SerializeField] private Text CheckpointMessage;
	
	private bool bCollision=false;
	private int flashCounter;
    private int currentHealthColorStep;

	public GameObject gameOver{ get{return _gameOver;} }
	public GameObject finishedGame{ get{return _finishedGame;} }
	public GameObject background{ get{return _background;} }
	public GameObject damageBackground{ get{return _damageBackground;} }
	public GameObject inGameUI{ get{return _inGameUI;} }
	public GameObject pause{ get{return _pause;} }

    void Awake ()
    {
        if (instance == null)
            instance = this;
        else
            gameObject.SetActive(false);

        GameMaster.OnGameStarted += new System.Action(InitUI);
    }

	void InitUI ()
    {
        gameOver.SetActive (false);
		finishedGame.SetActive (false);
		background.SetActive (false);
		pause.SetActive (false);
        _crosshair.enabled = false;
		damageBackground.SetActive(false);
        CheckpointMessage.enabled = false;

        inGameUI.SetActive(true);

		invertToggle.isOn = ApplicationManager.instance.invertedAim;
        currentHealthColorStep = 0;
        healthBarBase.color = fillColor.color = _healthColors[0].color;
        UpdateScore(GameMaster.instance.score);

        Submarine.OnHealthIncreased += new System.Action<float>(UpdateHealthBar);
        Submarine.OnHealthDecreased += new System.Action<float>(UpdateHealthBar);
        Submarine.OnHealthDecreased += new System.Action<float>(FlashScreen);
        Collectible.OnScoreChanged += new System.Action<int>(UpdateScore);
	}

    void OnDestroy()
    {
        Submarine.OnHealthIncreased -= new System.Action<float>(UpdateHealthBar);
        Submarine.OnHealthDecreased -= new System.Action<float>(UpdateHealthBar);
        Submarine.OnHealthDecreased -= new System.Action<float>(FlashScreen);
        Collectible.OnScoreChanged -= new System.Action<int>(UpdateScore);
        GameMaster.OnGameStarted -= new System.Action(InitUI);
    }

	void Update(){
		if (bCollision) {
			flashCounter--;
			if (flashCounter==0)
				damageBackground.SetActive(false);
		}
	}
	
    private void UpdateHealthBar(float newHealth)
    {
        scrollbar.value = 100.0f - newHealth;
        ChangeHealthColor(scrollbar.value);
    }

    private void UpdateScore(int newScore)
    {
        _score.text = newScore.ToString();
    }

    private void ChangeHealthColor(float newSliderValue)
    {
        for (int i = 0; i < _healthColors.Length - 1; ++i)
        {
            if (newSliderValue >= _healthColors[i].healthLevel &&
                newSliderValue < _healthColors[i+1].healthLevel)
            {
                healthBarBase.color = fillColor.color = _healthColors[i].color;
                return;
            }
        }

        if (newSliderValue >= _healthColors[_healthColors.Length-1].healthLevel)
            healthBarBase.color = fillColor.color = _healthColors[_healthColors.Length - 1].color;
    }

    public void ToggleCrosshair(bool visibility)
    {
        _crosshair.enabled = visibility;
    }

	public void FlashScreen(float useless)
	{
		flashCounter = 5;
		InGameUIManager.instance.damageBackground.SetActive (true);
		bCollision = true;
	}

    public void ResumeGame()
    {
        GameMaster.instance.networkView.RPC("TogglePause", RPCMode.All);
    }

    public void QuitToMenu()
    {
        ApplicationManager.instance.Disconnect(false);
    }

    public void RestartCheckpoint()
    {
        GameMaster.instance.RestartFromLastCheckpoint();
    }

    public void ReloadLevel()
    {
        GameMaster.instance.ReloadLevel();
    }

	public void A_ON(){
		A.sprite = A_On;
	}
	public void A_OFF(){
		A.sprite = A_Off;
	}
	public void X_ON(){
		X.sprite = X_On;
		X.color = new Color (X.color.r, X.color.g, X.color.b, 1.0f);
	}
	public void X_OFF(){
		X.sprite = X_Off;
		X.color = new Color (X.color.r, X.color.g, X.color.b, 1.0f);
	}
	public void X_No_Module(){
		X.sprite = X_NoModule;
		X.color = new Color (X.color.r, X.color.g, X.color.b, 0.1f);
	}
	public void B_ON(){
		B.sprite = B_On;
		B.color = new Color (B.color.r, B.color.g, B.color.b, 1.0f);
	}
	public void B_OFF(){
		B.sprite = B_Off;
		B.color = new Color (B.color.r, B.color.g, B.color.b, 1.0f);
	}
	public void B_No_Module(){
		B.sprite = B_NoModule;
		B.color = new Color (B.color.r, B.color.g, B.color.b, 0.1f);
	}
	public void Y_ON(){
		if(Network.isServer)
			Y.sprite = Y_Light_On;
		else
			Y.sprite = Y_Flare_On;
		Y.color = new Color (Y.color.r, Y.color.g, Y.color.b, 1.0f);
	}
	public void Y_OFF(){
		if(Network.isServer)
			Y.sprite = Y_Light_Off;
		else
			Y.sprite = Y_Flare_Off;
		Y.color = new Color (Y.color.r, Y.color.g, Y.color.b, 1.0f);
	}
	public void Y_No_Module(){
		Y.sprite = Y_NoModule;
		Y.color = new Color (Y.color.r, Y.color.g, Y.color.b, 0.1f);
	}

    public void ShowCheckpointMesesage()
    {
        StartCoroutine(ShowCheckpointMesesage(2.0f));
    }

    private IEnumerator ShowCheckpointMesesage(float duration)
    {
        CheckpointMessage.enabled = true;
        yield return new WaitForSeconds(duration);
        CheckpointMessage.enabled = false;
    }

    public void ShowGameOver()
    {
        gameOver.SetActive(true);
        //background.SetActive(true);
        inGameUI.SetActive(false);
        ToggleCrosshair(false);

		EventSystem.current.SetSelectedGameObject(GameObject.Find("/UI/GameOver/Reload Checkpoint"));
    }

    public void ShowFinishedGame(bool isHighScore)
    {
        finishedGame.SetActive(true);
        //background.SetActive(true);
        inGameUI.SetActive(false);

        #region Leaderboard
        if (isHighScore)
            _endGameText.text = "New Highscore!";
        else
            _endGameText.text = "";

        _endGameScore.text = "Your score is:\n" + GameMaster.instance.score.ToString();

        TimeSpan endTime = TimeSpan.FromSeconds(GameMaster.instance.endTime);

        _endGameTime.text = string.Format("Your time is:\n{0:D2} : {1:D2} : {2:D3}",
                endTime.Minutes,
                endTime.Seconds,
                endTime.Milliseconds);

        for (int i = 0; i < LeaderboardManager.scores.Count && i < 10; ++i)
        {
            TimeSpan time = TimeSpan.FromSeconds(LeaderboardManager.scores[i].time);
            _scoreListLines[i].transform.Find("Score+Partner/Time").GetComponent<Text>().text = string.Format("{0:D2}:{1:D2}:{2:D3}",
                time.Minutes,
                time.Seconds,
                time.Milliseconds);
            _scoreListLines[i].transform.Find("Score+Partner/Score").GetComponent<Text>().text = LeaderboardManager.scores[i].points.ToString();
            _scoreListLines[i].transform.Find("Score+Partner/Partner").GetComponent<Text>().text = LeaderboardManager.scores[i].partnerName;
            _scoreListLines[i].SetActive(true);
        }

        #endregion

        EventSystem.current.SetSelectedGameObject(GameObject.Find("/UI/EndGame/Retry"));
    }

    public void TogglePauseMenu(bool toggle)
    {
        pause.SetActive(toggle);
        //background.SetActive(toggle);
        inGameUI.SetActive(!toggle);

        EventSystem.current.SetSelectedGameObject(GameObject.Find("/UI/Pause/InvertedAim"));
    }

	public void ReverseApplicationAim(){
		ApplicationManager.instance.ReverseAim ();
	}

}
