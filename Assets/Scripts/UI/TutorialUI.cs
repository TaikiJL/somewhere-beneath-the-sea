﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialUI : MonoBehaviour {

    public static TutorialUI instance { get; private set; }

	//Strings to attach to tutorials.
	[SerializeField] private string tutoTorpedo;
	[SerializeField] private string tutoRepair;
	[SerializeField] private string tutoFlare;
	[SerializeField] private string tutoLight;
    [SerializeField] private string tutoPropeller;
    [SerializeField] private string tutoLightTwo;
	private string voidText = "";

	//Sprites used with the image
    [SerializeField] private Sprite A;
	[SerializeField] private Sprite B;
	[SerializeField] private Sprite X;
	[SerializeField] private Sprite Y_Flare;
	[SerializeField] private Sprite Y_Light;
    [SerializeField] private Sprite RT;
    [SerializeField] private Sprite JStick;
    [SerializeField] private Sprite JStick_L;
    [SerializeField] private Sprite JStick_R;
    [SerializeField] private Sprite JStick_U;
    [SerializeField] private Sprite JStick_D;

	[SerializeField] private Sprite Void_Sprite;

    //Font that we want to use
    [SerializeField] private Font myFont;

    //Image & text in the ui the scripts refer to
	public Image TutoImage;
    public Image TutoImage_L;
    public Image TutoImage_R;
    public Image TutoImage_D;
	public Text  TutoText;
    public Text  TutoText_L;
    public Text  TutoText_R;
    public Text  TutoText_D;

    //Time we want the tutorial text/image to last on screen
	public float timeShown = 3.0f;

    //Needs to be acessed in another script, but we dont want this to be modified in the editor.
    [HideInInspector] public bool torpedo = false;
    [HideInInspector] public bool repair = false;
    [HideInInspector] public bool flare = false;
    [HideInInspector] public bool light = false;
    [HideInInspector] public bool propeller = true;

	private bool timing = false;
	private float timer = 0.0f;
    private bool isTutoLight = false;
    //Game master reference to make sure tutorials arent repeated when, say, we restart during the same game.
    

	

    // Use this for initialization
	void Awake () {
        if (instance == null)
            instance = this;
        else
            gameObject.SetActive(false);

		TutoText.text = "";
		TutoText.font = myFont;
        TutoText_L.text = "";
		TutoText_L.font = myFont;
        TutoText_R.text = "";
		TutoText_R.font = myFont;
        TutoText_D.text = "";
		TutoText_D.font = myFont;
	}
	
	// Update is called once per frame
	void Update () {
		if (timing == true)
			timer = timer + Time.deltaTime;

		if (timer >= timeShown) {
            if (isTutoLight)
            {
                isTutoLight = false;
                timer = 0.0f;
                TutoText.text = tutoLightTwo;
            }

            else
            {
                timer = 0.0f;
                timing = false;
                torpedo = false;
                repair = false;
                flare = false;
                light = false;
                propeller = false;
                TutoText.text = "";
                TutoText_L.text = "";
                TutoText_R.text = "";
                TutoText_D.text = "";
                TutoImage.sprite = Void_Sprite;
                TutoImage_L.sprite = Void_Sprite;
                TutoImage_R.sprite = Void_Sprite;
                TutoImage_D.sprite = Void_Sprite;
            }
		}

	}

	void OnGUI()
	{
        //Shows a tutorial at the begining of the game for propellers.
        if (propeller == true && GameMaster.instance.firstTimePropeller == true)
        {
            timer = 0.0f;
            propeller = false;
            timing = true;
            TutoImage.sprite = A;
            TutoImage_D.sprite = RT;
            TutoText.text = tutoPropeller;
            TutoText_L.text = "Move";
            TutoText_R.text = "Move";
            TutoText_D.text = "Speed up";
            if (Network.isServer)
            {
                TutoImage_L.sprite = JStick_L;
                TutoImage_R.sprite = JStick_R;
            }
            else
            {
                TutoImage_L.sprite = JStick_U;
                TutoImage_R.sprite = JStick_D;
            }
            GameMaster.instance.firstTimePropeller = false;
        }
        //Shows a tutorial for the torpedo
        if (torpedo == true)
        {
            timer = 0.0f;
            torpedo = false;
			timing = true;
            TutoImage.sprite = B;
            TutoImage_D.sprite = RT;
			TutoText.text = tutoTorpedo;
            TutoText_L.text = "Aim";
            TutoText_R.text = "Aim";
            TutoText_D.text = "Shoot";
            TutoImage_L.sprite = JStick;
            TutoImage_R.sprite = JStick;
        
		}
        //Shows a tutorial for the repair
        if (repair == true )
        {
            timer = 0.0f;
			timing = true;
            TutoImage.sprite = X;
            TutoImage_D.sprite = RT;
            TutoImage_L.sprite = Void_Sprite;
            TutoImage_R.sprite = Void_Sprite;
            TutoText.text = tutoRepair;
            TutoText_D.text = "Repair";
            TutoText_L.text = "";
            TutoText_R.text = "";
            repair = false;
            

		}
        //Determines who we are looking at and show the tutorial for the corresponding module
        if (light == true)
        {
            //Things that are the same for both players
            timer = 0.0f;
            light = false;
			timing = true;
            isTutoLight = true;
            TutoText_L.text = "Aim";
            TutoText_R.text = "Aim";
            TutoImage_L.sprite = JStick;
            TutoImage_R.sprite = JStick;
            TutoImage_D.sprite = RT;

            //Player one gets Light tutorial
			if (Network.isServer)
			{
              
				TutoText.text = tutoLight;
				TutoImage.sprite = Y_Light;
                TutoText_D.text = "Light";
			}
            //Player two gets flare tutorial
			else 
			{
                
				TutoText.text = tutoFlare;
				TutoImage.sprite = Y_Flare;
                TutoText_D.text = "Shoot";
			}

        
		}
	
	}
}


