﻿using UnityEngine;
using System.Collections;
//using UnityEngine.UI;

public class DebugGUIMenu : MonoBehaviour {

    enum menu { MAIN, HOST, SERVERLIST, CLIENT, INGAME, NONE };

    private menu currentMenu = menu.MAIN;

    private string serverAdress;

    private bool displayGUI = false;
    private bool isServerLocal = false;

    //Text moduleName;

    void Start()
    {
        serverAdress = "127.0.0.1";

        //moduleName = GameObject.Find("Text").GetComponent<Text>();
    }

    void FixedUpdate()
    {
        //moduleName.text = Submarine.currentModuleName;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.PageDown))
            displayGUI = !displayGUI;
    }

	void OnGUI()
    {
        if (!displayGUI)
            return;

        switch (currentMenu)
        {
            case menu.MAIN:
                MainMenu();
                break;
            case menu.HOST:
                HostMenu();
                break;
            case menu.SERVERLIST:
                ServerListMenu();
                break;
            case menu.CLIENT:
                ClientMenu();
                break;
            case menu.INGAME:
                IngameMenu();
                break;
            default:
                break;
        }
    }

    void MainMenu()
    {
        if (GUILayout.Button("Host"))
        {
            currentMenu = menu.HOST;
            NetworkManager.HostLocalServer();
        }

        serverAdress = GUILayout.TextField(serverAdress);

        GUILayout.BeginHorizontal();
        
        if (GUILayout.Button("Join"))
        {
            currentMenu = menu.INGAME;
            NetworkManager.JoinServer(serverAdress);
        }

        if (GUILayout.Button("Local Server")){
            serverAdress = "127.0.0.1";
			isServerLocal = true;
		}

        GUILayout.EndHorizontal();
    }

    void HostMenu()
    {
        GUILayout.Label(Network.connections.Length + " players are connected");

        if (GUILayout.Button("Start game"))
        {
            currentMenu = menu.INGAME;
			ApplicationManager.instance.LoadLevel(1);
        }
        if (GUILayout.Button("Back"))
        {
            currentMenu = menu.MAIN;
            NetworkManager.Disconnect();
        }
            
    }

    void ServerListMenu()
    {
        HostData[] data = MasterServer.PollHostList();

        foreach (HostData element in data)
        {
            GUILayout.BeginHorizontal();
            string name = element.gameName + " " + element.connectedPlayers + " / " + element.playerLimit;
            GUILayout.Label(name);
            GUILayout.Space(5);
            string hostInfo;
            hostInfo = "[";
            foreach (string host in element.ip)
                hostInfo = hostInfo + host + ":" + element.port + " ";
            hostInfo = hostInfo + "]";
            GUILayout.Label(hostInfo);
            GUILayout.Space(5);
            GUILayout.Label(element.comment);
            GUILayout.Space(5);
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Connect"))
            {
                currentMenu = menu.CLIENT;

                // Connect to HostData struct, internally the correct method is used (GUID when using NAT).
                NetworkManager.JoinServer(element);
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Refresh"))
            NetworkManager.RequestServerList();
        if (GUILayout.Button("Join local server"))
        {
            currentMenu = menu.INGAME;
            NetworkManager.JoinServer("127.0.0.1");
        }
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Back"))
        {
            currentMenu = menu.MAIN;
        }
    }

    void ClientMenu()
    {

    }

    void IngameMenu()
    {
        GUILayout.Label("Current module: " + Submarine.currentModuleName);
    }

    void OnDisconnectedFromServer()
    {
        currentMenu = menu.MAIN;
    }

}
