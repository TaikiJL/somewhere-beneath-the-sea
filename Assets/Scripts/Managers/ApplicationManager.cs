﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public class ApplicationManager : MonoBehaviour {

#if UNITY_EDITOR
	//A utiliser si le build est stuck dans une résolutiuon
	[MenuItem("Edit/Reset Playerprefs")] public static void DeletePlayerPrefs() { PlayerPrefs.DeleteAll(); }
#endif

    private const int MAIN_MENU_LEVEL = 0;

	public bool isTestingSolo=true;
	public static ApplicationManager instance { get; private set;}
	private byte loadedPlayers=0;
    private bool disconnectedFromLobby = false;
	internal bool invertedAim=false;

#if UNITY_EDITOR
	public bool isTestingLevel = false;

    public bool isFocused { get; private set; }

#endif

#if UNITY_EDITOR

    void OnApplicationFocus(bool focused)
    {
        isFocused = isTestingLevel ? true : focused;
    }

#endif

	void Awake()
    {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad(gameObject);
		} else {
            Destroy(gameObject);
		}
	}
	
	IEnumerator Start () {
#if UNITY_EDITOR
		if (isTestingLevel)
		{
			NetworkManager.HostLocalServer();

			while (GameMaster.instance == null)
				yield return null;

			GameMaster.instance.StartGame();
		}

        isFocused = true;

#endif
		yield return null;
	}

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift)
            && Input.GetKey(KeyCode.LeftControl)
            && Input.GetKey(KeyCode.LeftAlt)
            && Input.GetKeyDown(KeyCode.O))
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Key deleted");
        }
    }

    void OnDisconnectedFromServer()
    {
        if (disconnectedFromLobby)
        {
            disconnectedFromLobby = false;
            return;
        }

        Disconnection();
    }

    void OnPlayerDisconnected()
    {
        if (Application.loadedLevel != MAIN_MENU_LEVEL)
        {
            Disconnect();
        }
    }

	void OnLevelWasLoaded(int level)
    {
        if (instance != this)
            return;

		if (Network.isServer)
        {
			loadedPlayers++;
			StartCoroutine(CheckLoadedPlayers());
		}
        else if (Network.isClient)
        {
            networkView.RPC("NotifyLevelLoaded", RPCMode.Server);
		}
	}

    private void Disconnection()
    {
        loadedPlayers = 0;
        if (GameMaster.instance != null)
            Destroy(GameMaster.instance.gameObject);
        Network.SetLevelPrefix(MAIN_MENU_LEVEL);
        Application.LoadLevel(MAIN_MENU_LEVEL);
    }

    private IEnumerator CheckLoadedPlayers()
    {
        if (isTestingSolo)
            loadedPlayers++;

        while (loadedPlayers < 2 || GameMaster.instance == null)
            yield return null;

        networkView.RPC("StartGame", RPCMode.All);

        yield return null;
    }

    public void HostGame()
    {
		NetworkManager.HostServer();
    }

    public void LoadGame()
    {
        if (Network.connections.Length > 0)
            LoadLevel(1);
    }

    public void Disconnect(bool disconnectFromLobby = false)
    {
        disconnectedFromLobby = disconnectFromLobby;
        NetworkManager.Disconnect();
    }

	public void LoadLevel (int level)
    {
        networkView.RPC("LoadLevelRPC", RPCMode.All, level);
	}

    public void ApplicationQuit()
    {
        if (GamePadManager.instance != null)
            GamePadManager.instance.Vibrate(0.0f, 0.0f, 1.0f);
        Application.Quit();
    }

	[RPC] private void LoadLevelRPC(int level)
    {
		if (GamePadManager.instance != null)
			GamePadManager.instance.Vibrate(0.0f, 0.0f, 1.0f);
        Network.SetLevelPrefix(level);
		Application.LoadLevel(level);
	}

	[RPC] private void NotifyLevelLoaded()
    {
		loadedPlayers++;
	}
	
    [RPC] private void StartGame()
    {
		if (GamePadManager.instance != null)
			GamePadManager.instance.Vibrate(0.0f, 0.0f, 1.0f);
        GameMaster.instance.StartGame();
        loadedPlayers = 0;
    }

	public void ReverseAim(){
		invertedAim = !invertedAim;
	}

}
