﻿using UnityEngine;
using System.Collections;

public class LightScript : MonoBehaviour {

	[SerializeField]public Light lightComponent;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActivateLight(){
		lightComponent.light.enabled = true;
	}

	void OnTriggerEnter(Collider collision)
	{
		if (collision.CompareTag ("Flare"))
			lightComponent.light.enabled = true;
	}
}
